package main

import (
	goError "github.com/go-errors/errors"
	"github.com/graphql-go/handler"
	"grocery-backend/queries"
	"grocery-backend/security"
	"grocery-backend/types"
	"log"
	"net/http"

	"github.com/graphql-go/graphql"
)

func main() {
	types.InitDB()
	defer types.CloseDb()

	schemaConfig := graphql.SchemaConfig{
		Query: graphql.NewObject(graphql.ObjectConfig{
			Name:   "RootQuery",
			Fields: queries.GetRootFields(),
		}),
	}
	schema, err := graphql.NewSchema(schemaConfig)

	if err != nil {
		log.Printf("Failed to create new schema")
		log.Printf(goError.Wrap(err, 0).ErrorStack())
	}

	httpHandler := handler.New(&handler.Config{
		Schema: &schema,
		Pretty: true,
	})

	types.CustomGraphqlHandler("/", httpHandler)
	http.HandleFunc("/authenticate", security.CreateTokenEndpoint)
	log.Print("ready: listening...\n")

	http.ListenAndServe(":8080", nil)
}

package types

import (
	"github.com/graphql-go/graphql"
)

// Product type definition.
type Product struct {
	ID    int     `Db:"id" json:"id"`
	Label string  `Db:"label" json:"label"`
	Price float32 `Db:"price" json:"price"`
	Image string  `Db:"image_url" json:"image"`
}

// ProductType is the GraphQL schema for the product type.
var ProductType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Product",
	Fields: graphql.Fields{
		"id":    &graphql.Field{Type: graphql.ID},
		"label": &graphql.Field{Type: graphql.String},
		"price": &graphql.Field{Type: graphql.Float},
		"image": &graphql.Field{Type: graphql.String},
	},
})

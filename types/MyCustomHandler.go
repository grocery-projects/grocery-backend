package types

import (
	"context"
	"net/http"
	graphqlHandler "github.com/graphql-go/handler"
)

func CustomHandler(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET")
		w.Header().Set("Access-Control-Allow-Headers", "Accept, Authorization, Content-Type, Content-Length, Accept-Encoding")

		handler.ServeHTTP(w, r)
	})
}

func CustomHandlerFunc(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET")
		w.Header().Set("Access-Control-Allow-Headers", "Accept, Authorization, Content-Type, Content-Length, Accept-Encoding")

		next(w, r)
	})
}

func CustomGraphqlHandler(pattern string, handler *graphqlHandler.Handler) {
	http.HandleFunc(pattern, CustomHandlerFunc(func(response http.ResponseWriter, request *http.Request) {
		handler.ContextHandler(
			context.WithValue(context.Background(), "token", request.Header.Get("Authorization")),
			response,
			request,
		)
	}))
}
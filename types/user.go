package types

import "github.com/graphql-go/graphql"

// Product type definition.
type User struct {
	Id       string `json:"id"`
	Firstname string `json:"firstname"`
	Lastname string `json:"lastname"`
	BirthDate string `json:"birth_date"`
	Email string `json:"email"`
	PhoneNumber string `json:"phone_number"`
	Password string `json:"password"`
	AuthentificationTokenId string `json:"authentification_token_id"`
}

// ProductType is the GraphQL schema for the product type.
var UserType = graphql.NewObject(graphql.ObjectConfig{
	Name: "User",
	Fields: graphql.Fields{
		"id":    &graphql.Field{Type: graphql.ID},
		"firstname": &graphql.Field{Type: graphql.String},
		"lastname": &graphql.Field{Type: graphql.String},
		"birth_date": &graphql.Field{Type: graphql.String},
		"email": &graphql.Field{Type: graphql.String},
		"phone_number": &graphql.Field{Type: graphql.String},
		"password": &graphql.Field{Type: graphql.String},
		"authentification_token_id": &graphql.Field{Type: graphql.String},
	},
})
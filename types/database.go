package types

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	groceryerrors "grocery-backend/errors"
	"log"
	"os"
)

//TODO: Add dependency injection to make this variable private
var Db *sql.DB

func InitDB() {
	urlLink := fmt.Sprintf(
		"%s:%s@tcp(%s:%s)/%s",
		os.Getenv("DATA_BASE_USER"),
		os.Getenv("DATA_BASE_PASSWORD"),
		os.Getenv("DATA_BASE_URL"),
		os.Getenv("DATA_BASE_PORT"),
		os.Getenv("DATA_BASE_NAME"),
	)
	Db, _ = sql.Open(fmt.Sprintf("%s", os.Getenv("DATA_BASE_DRIVER")), urlLink)
}

func GetDb() (*sql.DB, error) {
	if Db == nil {
		log.Printf("No Database Connection")
		return nil, groceryerrors.NewGraphQlError(groceryerrors.DATABASE_ERROR, "No Database Connection", nil)
	}
	return Db, nil
}

func CloseDb() {
	if Db != nil {
		err := Db.Close()
		if err != nil {
			log.Printf("%e", err)
		}
	}
}

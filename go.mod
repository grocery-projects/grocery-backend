module grocery-backend

require (
	github.com/dgrijalva/jwt-go v3.0.0+incompatible
	github.com/go-errors/errors v1.0.1
	github.com/go-sql-driver/mysql v1.4.0
	github.com/graphql-go/graphql v0.6.0
	github.com/graphql-go/handler v0.1.0
	github.com/kr/pretty v0.1.0 // indirect
	github.com/lib/pq v1.0.0
	github.com/mattn/go-sqlite3 v1.10.0
	github.com/mitchellh/mapstructure v1.1.2
	golang.org/x/net v0.0.0-20170529214944-3da985ce5951
)

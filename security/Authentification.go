package security

import (
	"fmt"
	"grocery-backend/database"
	"log"
)

func ValidatePassword(email string, password string) bool {
	var total int
	condition := fmt.Sprintf("WHERE email='%s' AND user_password='%s'", email, hashPassword(password))
	rows, err := database.GetQueryManagerInstance().SelectCondition("users", condition, "count(*) total")
	if err != nil {
		log.Printf("%e", err)
		return false
	}
	defer rows.Close()
	for rows.Next() {
		err = rows.Scan(&total)
		if err != nil {
			log.Fatal(err)
		}
		log.Printf("total of rows: %d", total)
		return total == 1
	}
	return false
}

func hashPassword(password string) string {
	//TODO: Hash password with same hash as database
	return password
}

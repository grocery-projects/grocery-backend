package security

import (
	"encoding/json"
	"errors"
	"fmt"
	"grocery-backend/database"
	"grocery-backend/types"
	cust_error "grocery-backend/errors"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/mitchellh/mapstructure"
)

const (
	JWT_STATUS_ACTIF string = "ACTIF"
	JWT_STATUS_EXPIRED string = "EXPIRED"
	JWT_STATUS_SUSPENDED string = "SUSPENDED"
)

type JwtToken struct {
	Token string `json:"token"`
	Status string `json:"status"`
}

func IsAccessAuthorized(jwtToken string) (bool, error) {
	if jwtToken != "" {
		bearerToken := strings.Split(jwtToken, " ")
		if len(bearerToken) == 2 {
			result, err := isJwtValid(bearerToken[1])
			if err != nil {
				return result, err
			}
			result, err = isJwtEnabled(bearerToken[1])
			if err != nil {
				return result, err
			}
			return result, nil
		} else {
			return false, errors.New(cust_error.INVALID_JWT_FORMAT)
		}
	} else {
		return false, errors.New(cust_error.AUTHORIZATION_HEADER_REQUIRED)
	}
}

func isJwtValid(jwtToken string) (bool, error) {
	var user types.User

			token, err := jwt.Parse(jwtToken, func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("There was an error")
				}
				return []byte(os.Getenv("JWT_SECRET_KEY")), nil
			})
			if err != nil {
				return false, err
			}
			if token.Valid {
				err := mapstructure.Decode(token.Claims.(jwt.MapClaims), &user)
				return true, err
			} else {
				return false, errors.New(cust_error.INVALID_JWT_TOKEN)
			}

}

func isJwtEnabled(jwtToken string) (bool, error) {
	condition := fmt.Sprintf("WHERE value='%s'", jwtToken)
	rows, err := database.GetQueryManagerInstance().SelectCondition("authentification_token", condition, "status")
	defer rows.Close()
	if err != nil {
		log.Printf("%e", err);
		return false, nil
	}
	for rows.Next() {
		var (
			jwtStatus    string
		)
		err = rows.Scan(&jwtStatus)
		if err != nil {
			log.Printf("Error during retreiving data from database")
			return false, err
		}
		switch jwtStatus {
			case JWT_STATUS_ACTIF:
				return true, nil
			case JWT_STATUS_EXPIRED:
				return false, errors.New(cust_error.JWT_EXPIRED)
			case JWT_STATUS_SUSPENDED:
				return false, errors.New(cust_error.JWT_SUSPENDED)
		}
	}
	log.Printf("No rows found")
	return false, errors.New(cust_error.NO_RAWS_FOUND)
}

func createToken(user types.User) (string, error) {
	if len(user.Email) == 0 || len(user.Password) == 0 {
		return "", errors.New(fmt.Sprintf("{\"errorType\":\"%s\", \"message\": \"%s\"", cust_error.INVALID_JWT_PARAMETER, "Missing email or password"))
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"email": user.Email,
		"password": user.Password,
	})
	return token.SignedString([]byte(os.Getenv("JWT_SECRET_KEY")))
}

func CreateTokenEndpoint(w http.ResponseWriter, req *http.Request) {
	var user types.User
	err := json.NewDecoder(req.Body).Decode(&user)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		_, _ = w.Write([]byte("{\"error\":\"400 - Empty body\"}"))
		return
	}
	if len(user.Email) == 0 || len(user.Password) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		_, _ = w.Write([]byte("{\"error\":\"400 - missing email or password\"}"))
		return
	}
	tokenString, erro := createToken(user)
	if erro != nil {
		log.Printf("%e", erro)
		return
	}
	err = json.NewEncoder(w).Encode(JwtToken{Token: tokenString, Status: JWT_STATUS_ACTIF})
	if err != nil {
		log.Printf("%e", erro)
		return
	}
}

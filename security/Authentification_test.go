package security

import (
	"database/sql"
	"fmt"
	"grocery-backend/database"
	_ "github.com/mattn/go-sqlite3"
	"grocery-backend/types"
	"os"
	"testing"
)

const DATABASE_PATH = "../mocked_db.db"

func InitMockedDb() {
	db, err := sql.Open("sqlite3", DATABASE_PATH)
	if err != nil {
		fmt.Printf("Fail to init mocked database")
	}
	types.Db = db
	database.GetQueryManagerInstance().GenericPrepare("CREATE TABLE IF NOT EXISTS `users` (`id` int NOT NULL,`firstname` varchar(50) NOT NULL,`lastname` varchar(50) NOT NULL,`user_password` varchar(50) NOT NULL,`email` varchar(100) NOT NULL UNIQUE,`phone_number` varchar(10) NOT NULL UNIQUE,`authentification_token_id` int NOT NULL UNIQUE)")
	database.GetQueryManagerInstance().GenericPrepare("CREATE TABLE IF NOT EXISTS `authentification_token` (`id` int(11) NOT NULL,`value` varchar(200) NOT NULL UNIQUE,`expiration_date` datetime NOT NULL, `status` varchar(20) NOT NULL)")
}

func CloseMockedDb() {
	defer types.CloseDb()
	os.Remove(DATABASE_PATH)
}

func TestValidatePassword(t *testing.T) {
	InitMockedDb()
	defer CloseMockedDb()
	database.GetQueryManagerInstance().Insert("users", "1", "John", "Snow", "mypassword", "john.snow@hotmail.fr", "0665635880", "jwt")
	validated := ValidatePassword("john.snow@hotmail.fr", "mypassword")
	if !validated {
		t.Errorf("email/password not validated")
	}
}

func TestValidateWrongPassword(t *testing.T) {
	InitMockedDb()
	defer CloseMockedDb()
	database.GetQueryManagerInstance().Insert("users", "1", "John", "Snow", "mypassword", "john.snow@hotmail.fr", "0665635880", "jwt")
	validated := ValidatePassword("john.snow@hotmail.fr", "mypassword2")
	if validated {
		t.Errorf("email/password not validated")
	}
}
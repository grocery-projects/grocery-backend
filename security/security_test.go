package security

import (
	"fmt"
	"grocery-backend/database"
	"grocery-backend/errors"
	"log"
	"os"
	"testing"
)

const COVERAGE_THRESHOLD  = 0.5

func TestMain(m *testing.M) {
	// call flag.Parse() here if TestMain uses flags
	rc := m.Run()

	// rc 0 means we've passed,
	// and CoverMode will be non empty if run with -cover
	if rc == 0 && testing.CoverMode() != "" {
		c := testing.Coverage()
		if c < COVERAGE_THRESHOLD {
			log.Println(fmt.Sprintf("Tests passed but coverage failed at %.2f%s", c, "%"))
			rc = -1
		}
	}
	os.Exit(rc)
}

func TestIsJwtEnabled(t *testing.T) {
	InitMockedDb()
	defer CloseMockedDb()
	database.GetQueryManagerInstance().Insert("authentification_token", "1", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImpvaG4uc25vd0Bob3RtYWlsLmZyIiwicGFzc3dvcmQiOiJteXBhc3N3b3JkIn0.c1buy0v0Q-Ff3i6CKWheilusTkgmR-lJH8xjH66lfi4", "9999-12-31 23:59:59", JWT_STATUS_ACTIF)
	enabled, err := isJwtEnabled("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImpvaG4uc25vd0Bob3RtYWlsLmZyIiwicGFzc3dvcmQiOiJteXBhc3N3b3JkIn0.c1buy0v0Q-Ff3i6CKWheilusTkgmR-lJH8xjH66lfi4")
	if !enabled {
		t.Errorf("jwt not enabled")
	}
	if err != nil {
		t.Errorf("Unexpected error %e", err)
	}
}

func TestIsJwtEnabledWrongJwt(t *testing.T) {
	InitMockedDb()
	defer CloseMockedDb()
	database.GetQueryManagerInstance().Insert("authentification_token", "1", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImpvaG4uc25vd0Bob3RtYWlsLmZyIiwicGFzc3dvcmQiOiJteXBhc3N3b3JkIn0.c1buy0v0Q-Ff3i6CKWheilusTkgmR-lJH8xjH66lfi4", "9999-12-31 23:59:59", JWT_STATUS_ACTIF)
	enabled, err := isJwtEnabled("wrongjswt")
	if enabled {
		t.Errorf("jwt enabled")
	}
	if err == nil {
		t.Errorf("An error was expected, because no row should be returned")
	}
	if err != nil && err.Error() != errors.NO_RAWS_FOUND {
		t.Errorf("Expected error NO_RAWS_FOUND but was %s", err.Error())
	}
}

func TestIsJwtDisabledJwt(t *testing.T) {
	InitMockedDb()
	defer CloseMockedDb()
	database.GetQueryManagerInstance().Insert("authentification_token", "1", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImpvaG4uc25vd0Bob3RtYWlsLmZyIiwicGFzc3dvcmQiOiJteXBhc3N3b3JkIn0.c1buy0v0Q-Ff3i6CKWheilusTkgmR-lJH8xjH66lfi4", "9999-12-31 23:59:59", JWT_STATUS_EXPIRED)
	enabled, err := isJwtEnabled("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImpvaG4uc25vd0Bob3RtYWlsLmZyIiwicGFzc3dvcmQiOiJteXBhc3N3b3JkIn0.c1buy0v0Q-Ff3i6CKWheilusTkgmR-lJH8xjH66lfi4")
	if enabled {
		t.Errorf("jwt enabled")
	}
	if err == nil || err.Error() != errors.JWT_EXPIRED {
		t.Errorf("Expected error JWT_EXPIRED but was %e", err)
	}
}

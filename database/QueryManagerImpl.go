package database

import (
	"database/sql"
	default_error "errors"
	"fmt"
	_ "github.com/lib/pq"
	"grocery-backend/errors"
	"grocery-backend/types"
	"log"
	"strings"
	"sync"
)

type queryManagerImpl struct {
}

var instance *queryManagerImpl
var once sync.Once

func GetQueryManagerInstance() *queryManagerImpl {
	once.Do(func() {
		instance = &queryManagerImpl{}
	})
	return instance
}

func (queryManagerImpl queryManagerImpl) Select(table string, fields ...interface{}) (*sql.Rows, error) {
	return queryManagerImpl.genericSelect(table, "", fields)
}

func (queryManagerImpl queryManagerImpl) SelectCondition(table string, condition string, fields ...interface{}) (*sql.Rows, error) {
	return queryManagerImpl.genericSelect(table, condition, fields)
}

func (queryManagerImpl queryManagerImpl) genericSelect(table string, condition string, fields []interface{}) (*sql.Rows, error) {
	fieldsQuery := convertToStringArray(fields)
	q := fmt.Sprintf("SELECT %s FROM %s %s", fieldsQuery, table, condition)
	log.Printf(q)
	db, err := types.GetDb()
	if err != nil {
		return nil, err
	}
	rows, err := db.Query(q)
	if err != nil {
		return nil, errors.NewGraphQlError(errors.DATABASE_ERROR, fmt.Sprintf("Error during %s", q), err)
	}
	return rows, nil
}

func (queryManagerImpl queryManagerImpl) Insert(table string, values ...interface{}) error {
	queryValues, err := convertInterfaceIntoIntOrString(values)
	if err != nil {
		return errors.NewGraphQlError(errors.DATABASE_ERROR, fmt.Sprintf("Error during insert into %s using parameters %v", table, values), err)
	}
	var query = fmt.Sprintf("INSERT INTO %s VALUES (%s)", table, queryValues)
	log.Printf(query)
	return queryManagerImpl.GenericPrepare(query)
}

func (queryManagerImpl queryManagerImpl) GenericPrepare(query string) error {
	db, err := types.GetDb()
	if err != nil {
		return err
	}
	stmt, err := db.Prepare(query)
	if err != nil {
		log.Printf("Error during prepare of query %s", query)
		return errors.NewGraphQlError(errors.DATABASE_ERROR, fmt.Sprintf("Error during prepare of query %s", query), err)
	}
	log.Printf(query)
	_, err = stmt.Exec()
	if err != nil {
		log.Printf("Error during executing query %s", query)
		return errors.NewGraphQlError(errors.DATABASE_ERROR, fmt.Sprintf("Error during executing query %s", query), err)
	}
	return nil
}

func (queryManagerImpl queryManagerImpl) Update(table string, condition string, fields ...interface{}) error {
	fieldsQuery := convertToStringArray(fields)
	var query = fmt.Sprintf("UPDATE %s SET %s %s", table, fieldsQuery, condition)
	return queryManagerImpl.GenericPrepare(query)
}

func convertInterfaceIntoIntOrString(values []interface{}) (string, error) {
	var queryValues []string
	for _, v := range values {
		switch v.(type) {
		case int:
			queryValues = append(queryValues, fmt.Sprintf("%d", v))
			break
		case float32, float64:
			queryValues = append(queryValues, fmt.Sprintf("%.2f", v))
			break
		case string:
			queryValues = append(queryValues, fmt.Sprintf("\"%s\"", v))
			break
		default:
			return "", errors.NewGraphQlError(errors.SERVER_INTERNAL_ERROR, fmt.Sprintf("Wrong query parameter %v", v), default_error.New(fmt.Sprintf("Wrong query parameter %v", v)))
		}
	}
	return strings.Join(queryValues, ", "), nil
}

func convertToStringArray(values []interface{}) (string) {
	var queryValues []string
	for _, v := range values {
		queryValues = append(queryValues, fmt.Sprintf("%s", v))
	}
	return strings.Join(queryValues, ", ")
}

func CalculationOperationRequest(function string, column string) string {
	var result string
	rows, _ := GetQueryManagerInstance().Select("products", fmt.Sprintf("%s(%s) total", function, column))
	defer rows.Close()

	for rows.Next() {
		var (
			total string
		)
		err := rows.Scan(&total)
		if err != nil {
			log.Printf("Error during retreiving data from database")
			return ""
		}
		result += total
	}
	err := rows.Err()
	if err != nil {
		log.Printf("Rows return error")
		return ""
	}
	return result
}
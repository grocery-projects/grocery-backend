package database

import "database/sql"

type IQueryManager interface {
	Insert(table string, values ...interface{}) error
	Select(table string, fields ...interface{}) (*sql.Rows, error)
	SelectCondition(table string, condition string, fields ...interface{}) (*sql.Rows, error)
	Update(table string, condition string, fields ...interface{}) error
	GenericPrepare(query string) error
}

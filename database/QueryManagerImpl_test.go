package database

import (
	"database/sql"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"grocery-backend/types"
	"log"
	"os"
	"strconv"
	"testing"
)

const DATABASE_PATH = "../mocked_db.db"

func InitMockedDb() {
	db, err := sql.Open("sqlite3", DATABASE_PATH)
	if err != nil {
		fmt.Printf("Fail to init mocked database")
	}
	types.Db = db
	GetQueryManagerInstance().GenericPrepare("CREATE TABLE IF NOT EXISTS `products` (`id` int NOT NULL,`label` varchar(30) NOT NULL,`price` float NOT NULL)")
}

func CloseMockedDb() {
	defer types.CloseDb()
	os.Remove(DATABASE_PATH)
}

func TestQueryParameterConversion(t *testing.T) {
	var expectedValue = "1, \"label\", 1.00"
	s, _ := convertInterfaceIntoIntOrString([]interface{}{1, "label", 1.0})
	if s != expectedValue {
		t.Errorf("Conversion was incorrect, got: %s, want: %s.", s, expectedValue)
	}
}

func TestSelectAndInsert(t *testing.T) {
	var (
		id, expectedId       int
		label, expectedLabel string
		price, expectedPrice float32
	)
	expectedId = 1
	expectedLabel = "label"
	expectedPrice = 1.01

	InitMockedDb()
	defer CloseMockedDb()
	GetQueryManagerInstance().Insert("products", expectedId, expectedLabel, expectedPrice)

	rows, err := GetQueryManagerInstance().Select("products", "id", "label", "price")
	if err != nil {
		t.Errorf(err.Error())
	}
	defer rows.Close()

	for rows.Next() {
		err = rows.Scan(&id, &label, &price)
		if err != nil {
			log.Fatal(err)
		}
		log.Printf("id: %v, label: %s, price: %v", id, label, price)
		if id != expectedId {
			t.Errorf("id retreived was incorrect, got: %d, want: %d.", id, expectedId)
		} else if label != expectedLabel {
			t.Errorf("label retreived was incorrect, got: %s, want: %s.", label, expectedLabel)
		} else if price != expectedPrice {
			t.Errorf("price retreived was incorrect, got: %f, want: %f.", price, expectedPrice)
		}
	}
	err = rows.Err()
	if err != nil {
		t.Errorf(err.Error())
	}
}

func TestSelectCount(t *testing.T) {
	var total int
	InitMockedDb()
	defer CloseMockedDb()
	GetQueryManagerInstance().Insert("products", 1, "test1", 1.01)
	GetQueryManagerInstance().Insert("products", 2, "test1", 1.01)
	rows, err := GetQueryManagerInstance().Select("products", "count(*) total")
	if err != nil {
		t.Errorf(err.Error())
	}
	defer rows.Close()

	for rows.Next() {
		err = rows.Scan(&total)
		if err != nil {
			log.Fatal(err)
		}
		log.Printf("total of rows: %d", total)
		if total != 2 {
			t.Errorf("Total of rows retreived was incorrect, got: %d, want: %d.", total, 2)
		}
	}
	err = rows.Err()
	if err != nil {
		t.Errorf(err.Error())
	}
}

func TestSelectCondition(t *testing.T) {
	var (
		id, expectedId       int
		label, expectedLabel string
		price, expectedPrice float32
	)
	expectedId = 62
	expectedLabel = "label"
	expectedPrice = 1.01

	InitMockedDb()
	defer CloseMockedDb()
	GetQueryManagerInstance().Insert("products", expectedId, expectedLabel, expectedPrice)
	rows, err := GetQueryManagerInstance().SelectCondition("products", fmt.Sprintf("WHERE id = %d", expectedId), "id", "label", "price")
	if err != nil {
		t.Errorf(err.Error())
	}
	defer rows.Close()

	for rows.Next() {
		err = rows.Scan(&id, &label, &price)
		if err != nil {
			log.Fatal(err)
		}
		log.Printf("id: %v, label: %s, price: %v", id, label, price)
		if id != expectedId {
			t.Errorf("id retreived was incorrect, got: %d, want: %d.", id, expectedId)
		} else if label != expectedLabel {
			t.Errorf("label retreived was incorrect, got: %s, want: %s.", label, expectedLabel)
		} else if price != expectedPrice {
			t.Errorf("price retreived was incorrect, got: %f, want: %f.", price, expectedPrice)
		}
	}
	err = rows.Err()
	if err != nil {
		t.Errorf(err.Error())
	}
}

func TestUpdateCondition(t *testing.T) {
	var (
		id, expectedId       int
		label, expectedLabel string
		price, expectedPrice float32
	)
	expectedId = 62
	expectedLabel = "label"
	expectedPrice = 32

	InitMockedDb()
	defer CloseMockedDb()
	GetQueryManagerInstance().Insert("products", expectedId, expectedLabel, 1.62)
	GetQueryManagerInstance().Update("products", "WHERE id = 62", fmt.Sprintf("price = %f", expectedPrice))
	rows, err := GetQueryManagerInstance().SelectCondition("products", fmt.Sprintf("WHERE id = %d", expectedId), "id", "label", "price")
	if err != nil {
		t.Errorf(err.Error())
	}
	defer rows.Close()

	for rows.Next() {
		err = rows.Scan(&id, &label, &price)
		if err != nil {
			log.Fatal(err)
		}
		log.Printf("id: %v, label: %s, price: %v", id, label, price)
		if id != expectedId {
			t.Errorf("id retreived was incorrect, got: %d, want: %d.", id, expectedId)
		} else if label != expectedLabel {
			t.Errorf("label retreived was incorrect, got: %s, want: %s.", label, expectedLabel)
		} else if price != expectedPrice {
			t.Errorf("price retreived was incorrect, got: %f, want: %f.", price, expectedPrice)
		}
	}
	err = rows.Err()
	if err != nil {
		t.Errorf(err.Error())
	}
}

func TestCalculationOperationRequest(t *testing.T) {
	InitMockedDb()
	defer CloseMockedDb()
	GetQueryManagerInstance().Insert("products", 1, "test1", 1.01)
	GetQueryManagerInstance().Insert("products", 2, "test1", 1.01)
	product, _ := strconv.Atoi(CalculationOperationRequest("count", "*"))
	if product != 2 {
		t.Errorf("Total of rows retreived was incorrect, got: %d, want: %d.", product, 2)
	}
}
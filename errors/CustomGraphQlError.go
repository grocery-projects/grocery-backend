package errors

import (
	go_Error "github.com/go-errors/errors"
	graphql_error "github.com/graphql-go/graphql/gqlerrors"
)

func NewGraphQlError(errorType string, message string, err error) *graphql_error.Error{
	wrap := go_Error.Wrap(err, 1)
	return graphql_error.NewError(HttpErrorType {errorType, message}.Error(), nil, wrap.ErrorStack(), nil, nil, nil)
}

package errors

import "encoding/json"

// errorString is a trivial implementation of error.

func (e HttpErrorType) Error() string {
	bytes, _ := json.Marshal(e)
	return string(bytes)
}

type HttpErrorType struct {
	Type string `json:"type"`
	Payload string `json:"payload"`
}

package mutations

import (
	"github.com/graphql-go/graphql"
	"grocery-backend/types"
	"log"
)

// GetCreateProductMutation creates a new product and returns it.
func GetCreateProductMutation() *graphql.Field {
	return &graphql.Field{
		Type: types.ProductType,
		Args: graphql.FieldConfigArgument{
			"label": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			"price": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			log.Printf("[mutation] create product\n")

			product := &types.Product{
				Label: params.Args["label"].(string),
				Price: params.Args["price"].(float32),
			}

			return product, nil
		},
	}
}

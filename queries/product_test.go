package queries

import (
	"database/sql"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"grocery-backend/database"
	"grocery-backend/types"
	"os"
	"testing"
)
const DATABASE_PATH = "../mocked_db.db"

func InitMockedDb() {
	db, err := sql.Open("sqlite3", DATABASE_PATH)
	if err != nil {
		fmt.Printf("Fail to init mocked database")
	}
	types.Db = db
	database.GetQueryManagerInstance().GenericPrepare("CREATE TABLE IF NOT EXISTS `products` (`id` int NOT NULL,`label` varchar(30) NOT NULL,`price` float NOT NULL)")
}

func CloseMockedDb() {
	defer types.CloseDb()
	os.Remove(DATABASE_PATH)
}

func TestGetNumberOfProduct(t *testing.T) {
	InitMockedDb()
	defer CloseMockedDb()
	database.GetQueryManagerInstance().Insert("products", 1, "test1", 1.01)
	database.GetQueryManagerInstance().Insert("products", 2, "test1", 1.01)
	product := getNumberOfProduct()
	if product != 2 {
		t.Errorf("Total of rows retreived was incorrect, got: %d, want: %d.", product, 2)
	}
}
func TestGetMaxPrice(t *testing.T) {
	InitMockedDb()
	defer CloseMockedDb()
	database.GetQueryManagerInstance().Insert("products", 1, "test1", 1.01)
	database.GetQueryManagerInstance().Insert("products", 2, "test1", 40.09)
	product := getMaxPrice()
	if product != 40.09 {
		t.Errorf("Total of rows retreived was incorrect, got: %.2f, want: %.2f.", product, 40.09)
	}
}

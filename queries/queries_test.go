package queries

import (
	"fmt"
	"log"
	"os"
	"testing"
)

const COVERAGE_THRESHOLD  = 0.5

func TestMain(m *testing.M) {
	// call flag.Parse() here if TestMain uses flags
	rc := m.Run()

	// rc 0 means we've passed,
	// and CoverMode will be non empty if run with -cover
	if rc == 0 && testing.CoverMode() != "" {
		c := testing.Coverage()
		if c < COVERAGE_THRESHOLD {
			log.Println(fmt.Sprintf("Tests passed but coverage failed at %.2f%s", c, "%"))
			rc = -1
		}
	}
	os.Exit(rc)
}
package queries

import (
	"database/sql"
	default_error "errors"
	"fmt"
	"github.com/graphql-go/graphql"
	"grocery-backend/database"
	"grocery-backend/errors"
	"grocery-backend/types"
	"log"
	"strconv"
)

// GetProductQuery returns the queries available against product type.
func GetProductQueryFilter() *graphql.Field {
	return &graphql.Field{
		Type: graphql.NewList(types.ProductType),
		Args: graphql.FieldConfigArgument{
			"offset": &graphql.ArgumentConfig{
				Type: graphql.Int,
			},
			"minRaw": &graphql.ArgumentConfig{
				Type: graphql.Int,
			},
			"name": &graphql.ArgumentConfig{
				Type: graphql.String,
			},
			"maxPrice": &graphql.ArgumentConfig{
				Type: graphql.Float,
			},
			"minPrice": &graphql.ArgumentConfig{
				Type: graphql.Float,
			},
			"orderBy": &graphql.ArgumentConfig{
				Type: graphql.String,
			},
			"orderByDesc": &graphql.ArgumentConfig{
				Type: graphql.String,
			},
			"id": &graphql.ArgumentConfig{
				Type: graphql.ID,
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			var products []types.Product
			var rows *sql.Rows
			var err error
			var condition string
			if (params.Args["name"] != nil) {
				name := params.Args["name"].(string)
				condition += fmt.Sprintf("WHERE label LIKE '%s%s%s'", "%", name, "%")
			}
			if (params.Args["minPrice"] != nil || params.Args["maxPrice"] != nil) {
				condition = addMinMaxPriceCondition(condition, params)
			}
			if (params.Args["orderBy"] != nil || params.Args["orderByDesc"] != nil) {
				if (params.Args["orderBy"] != nil && params.Args["orderByDesc"] != nil) {
					return nil, errors.NewGraphQlError(errors.INVALID_REQUEST, "You can not order by and order by desc in the same request", default_error.New("You can not order by and order by desc in the same request"))
				}
				condition = addOrderByCondition(condition, params)
			}
			if (params.Args["offset"] != nil || params.Args["minRaw"] != nil) {
				condition = addMinMaxNumberCondition(condition, params)
			}
			if (params.Args["id"] != nil) {
				id := params.Args["id"].(string)
				condition = fmt.Sprintf("WHERE id=%s", id)
			}
			if (len(condition) > 0) {
				rows, err = database.GetQueryManagerInstance().SelectCondition("products", condition, "id", "label", "price", "image_url")
			} else {
				rows, err = database.GetQueryManagerInstance().Select("products", "id", "label", "price", "image_url")
			}
			if err != nil {
				return nil, err
			}
			defer rows.Close()

			for rows.Next() {
				var (
					id    int
					label string
					price float32
					image string
				)
				err = rows.Scan(&id, &label, &price, &image)
				if err != nil {
					log.Printf("Error during retreiving data from database")
					return nil, errors.NewGraphQlError(errors.DATABASE_ERROR, "Error during retreiving data from database", default_error.New("Error during retreiving data from database"))
				}
				products = append(products, types.Product{id, label, price, image})
			}
			err = rows.Err()
			if err != nil {
				log.Printf("Rows return error")
				return nil, errors.NewGraphQlError(errors.DATABASE_ERROR, "Rows return error", default_error.New("Rows return error"))
			}
			return products, nil
		},
	}
}

func addMinMaxNumberCondition(currentCondition string, params graphql.ResolveParams) string {
	var min, max int
	if (params.Args["offset"] == nil) {
		max = 0
	} else {
		max = params.Args["offset"].(int)
	}
	if (params.Args["minRaw"] == nil) {
		min = 0
	} else {
		min = params.Args["minRaw"].(int)
	}
	currentCondition += fmt.Sprintf(" LIMIT %d, %d", min, max)
	return currentCondition
}

func addOrderByCondition(currentCondition string, params graphql.ResolveParams) string {
	if (params.Args["orderBy"] != nil) {
		currentCondition += fmt.Sprintf(" ORDER BY %s", params.Args["orderBy"].(string))
	}
	if (params.Args["orderByDesc"] != nil) {
		currentCondition += fmt.Sprintf(" ORDER BY %s DESC", params.Args["orderByDesc"].(string))
	}
	return currentCondition
}

func addMinMaxPriceCondition(currentCondition string, params graphql.ResolveParams) string {
	var min, max float64
	if (params.Args["maxPrice"] == nil) {
		max = getMaxPrice()
	} else {
		max = params.Args["maxPrice"].(float64)
	}
	if (params.Args["minPrice"] == nil) {
		min = 0
	} else {
		min = params.Args["minPrice"].(float64)
	}
	return addNewWhereCondition(currentCondition, fmt.Sprintf(" price between '%.2f' AND '%.2f'", min, max))
}

func addNewWhereCondition(currentCondition string, conditionToAdd string) string {
	if (len(currentCondition) > 0) {
		currentCondition += " AND "
	} else {
		currentCondition += "WHERE "
	}
	currentCondition += conditionToAdd
	return currentCondition
}

func getNumberOfProduct() int {
	result ,_ :=strconv.Atoi(database.CalculationOperationRequest("COUNT", "*"))
	return result
}

func getMaxPrice() float64 {
	result ,_ :=strconv.ParseFloat(database.CalculationOperationRequest("MAX", "price"), 64)
	return result
}

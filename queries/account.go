package queries

import (
	"github.com/graphql-go/graphql"
	"grocery-backend/errors"
	"grocery-backend/security"
	"grocery-backend/types"
	"log"
)

func GetAccountQueryFilter() *graphql.Field {
	return &graphql.Field{
		Type: types.UserType,
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			accessAuthorized, err := security.IsAccessAuthorized(params.Context.Value("token").(string))
			if err != nil {
				log.Printf("%e", err)
			}
			if accessAuthorized {
				log.Println("Access Authorized")
				return &types.User{}, nil
			} else {
				log.Println("Access not Authorized")
				return nil, errors.NewGraphQlError(errors.UNAUTHORIZED_ACCESS, err.Error(), nil)
			}
		},
	}
}
